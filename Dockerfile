FROM python:latest

WORKDIR /root

COPY demo.py .

CMD ["python", "demo.py"]